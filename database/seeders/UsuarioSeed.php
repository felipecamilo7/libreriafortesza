<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
             'id_tipo_usuario' =>1
        ]);
        
        DB::table('users')->insert([
            'name' => 'felipe',
            'email' => 'felipe@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
            'id_tipo_usuario' =>2
        ]);
        DB::table('users')->insert([
            'name' => 'camilo',
            'email' => 'camilo@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
            'id_tipo_usuario' =>2
        ]);
        DB::table('users')->insert([
            'name' => 'paula',
            'email' => 'paula@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
            'id_tipo_usuario' =>2
        ]);
        DB::table('users')->insert([
            'name' => 'clara',
            'email' => 'clara@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
            'id_tipo_usuario' =>2
        ]);
        DB::table('users')->insert([
            'name' => 'sofia',
            'email' => '@gmail.com',
            'password' => Hash::make('1234'),
            'email_verified_at' => null,
            'id_tipo_usuario' =>2
        ]);
    }
}
