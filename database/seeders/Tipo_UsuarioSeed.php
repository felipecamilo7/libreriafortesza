<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Tipo_UsuarioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipo__usuarios')->insert([
            'name' => 'admin',
          
        ]);
        DB::table('tipo__usuarios')->insert([
            'name' => 'cliente',
          
        ]);
       

    }
}
