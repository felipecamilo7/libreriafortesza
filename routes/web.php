<?php

use App\Http\Controllers\ChatController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json(['mensaje' => 'Bienvenido al chat de la libreria fortesza']);
});


Route::middleware('auth:api')->group(function () {
    Route::get('/enviar', [ChatController::class, 'chat']); 
    
    Route::post('/chatArchivo', [ChatController::class, 'chatArchivo']);
    Route::get('/verUsuario',[UserController::class,'verUsuario']);
  
    Route::get('/verChats',[ChatController::class,'verChats'] );
    Route::get('/verMensajeUsuario',[ChatController::class, 'verChatDeUsuario']);

});
Route::view('/verTest', 'recibir');



Route::get('/login',[UserController::class,'login'] )->name('login');



    

Route::post('/logout',[UserController::class,'logout']);






