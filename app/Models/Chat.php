<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    public function usuario()
    {
        return $this->belongsTo(User::class,'id_user_reseptor','id');
    }
    public function archivo()
    {
        return $this->belongsTo(Archivo::class,'id_archivos','id');
    }
}
