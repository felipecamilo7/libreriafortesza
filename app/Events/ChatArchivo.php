<?php

namespace App\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatArchivo implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $emisor;
    public $receptor;
    public $nombre_archivo;
    public $ruta;
 
  
    public function __construct($emisor ,$message , $receptor , $nombre_archivo ,$ruta)
    {
        $this->message = $message;
        $this->emisor = $emisor;
        $this->receptor = $receptor;
        $this->nombre_archivo = $nombre_archivo;
        $this->ruta = $ruta;
    

        
    }
  
    public function broadcastOn()
    {
        return ['my-channel'];
    }
  
    public function broadcastAs()
    {
        return 'my-event';
    }
}
