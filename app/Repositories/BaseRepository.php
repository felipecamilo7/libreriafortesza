<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }
    public function new()
    {
        return new $this->model();
    }


    public function all() {
        return $this->model->all();
    }

    public function find($id) {
        return $this->model->find($id);
    }

    public function save(Model $model)
    {
        $model->save();

        return $model;
    }

    public function update($id, array $data) {
        $user = $this->model->find($id);
        $user->update($data);
        return $user;
    }

    public function delete($id) {
        $user = $this->model->find($id);
        return $user->delete();
    }
}