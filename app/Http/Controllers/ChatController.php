<?php

namespace App\Http\Controllers;

use App\Events\ChatArchivo;
use App\Events\ChatNormal;
use App\Models\Archivo;
use App\Models\Chat;
use App\Models\User;
use App\Repositories\ArchivoRepository;
use App\Repositories\ChatRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
/**
* @OA\Info(title="API Chat", version="1.0")
*
* @OA\Server(url="http://swagger.local")
*/


class ChatController extends Controller
{
    //
    private  $chatRepository;
    private  $archivoRepository;
    private  $userRepository;
    public function __construct(ChatRepository $chatRepository, ArchivoRepository $archivoRepository , UserRepository $userRepository)
    {
        $this->chatRepository = $chatRepository;
        $this->archivoRepository = $archivoRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
 * @OA\Get(
 *     path="/enviar",
 *     summary="mostrar chat archivo",
 *   
 *     @OA\Response(
 *         response=200,
 *         description="mostrar chat archivo",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Validation error",
 *     ),
 * )
 */
    
    public function chat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'mensaje' => 'required|string|max:500',
        ]);
    
        if ($validator->fails()) {
            return response()->json([
                'message' => 'los datos enviados no son validos',
                'errors' => $validator->errors(),
            ], 422);
        }



        $emisor = $request->user();

        $reseptor = $this->userRepository->find($request->id);
        $mensaje = $request->mensaje;

        


       
        $chat = $this->chatRepository->new();
        $chat->id_user_reseptor = $reseptor->id;
        $chat->emisor = $emisor->name;
        $chat->mensaje = $mensaje;
        $this->chatRepository->save($chat);
        
        event(new ChatNormal($emisor, $mensaje, $reseptor->name));
         


        return response()->json(['mensaje' => 'Mensaje enviado']);
    }
    /**
    * @OA\Get(
    *     path="/verTest",
    *     summary="ver test de las vistas",
    *     @OA\Response(
    *         response=200,
    *         description="ver test de las vistas"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    

    public function recibirVista()
    {

        return view('recibir');
    }
    /**
    * @OA\Get(
    *     path="/verMensajeUsuario",
    *     summary="ver Chat De Usuario",
    *     @OA\Response(
    *         response=200,
    *         description="ver chat de usuario"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function verChatDeUsuario(Request $request)
    {
        
        $user = $request->user();
        $chat =  User::with('chat')->where('id', $user->id)->paginate(15);
        return $chat;
    }
     /**
    * @OA\Get(
    *     path="/verChats",
    *     summary="ver todos los chats",
    *     @OA\Response(
    *         response=200,
    *         description="ver todos los chats"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function verChats(){
       $chat =  Chat::with('usuario','archivo')->paginate(15);
       return $chat;

    }
    /**
 * @OA\Post(
 *     path="/chatArchivo",
 *     summary="enviar archivos en el chat",
 *   
 *     @OA\Response(
 *         response=200,
 *         description="enviar archivos en el chat",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Validation error",
 *     ),
 * )
 */
    
    public function chatArchivo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'mensaje' => 'required|string|max:500',
            'archivo' => 'required|file'
        ]);
    
        if ($validator->fails()) {
            return response()->json([
                'message' => 'los datos enviados no son validos',
                'errors' => $validator->errors(),
            ], 422);
        }
        
        $user_emisor = $request->user();
        $user_receptor =  $this->userRepository->find($request->id);
       

        $file = $request->file('archivo');
        $ruta = Storage::putFile('archivos', $file);
        $archivo = $this->archivoRepository->new();
        $archivo->nombre = $file->getClientOriginalName();
        $archivo->ruta = $ruta;
        $this->archivoRepository->save($archivo);
        
        $chat = $this->chatRepository->new();
        $chat->id_user_reseptor = $user_receptor->id ;
        $chat->emisor = $user_emisor->name;
        $chat->mensaje = $request->mensaje;
        $chat->id_archivos = $archivo->id;
        $this->chatRepository->save($chat);
        event(new ChatArchivo($user_emisor->name, $chat->mensaje,  $user_receptor->name,$archivo->nombre,$archivo->ruta));


        


        return response()->json(['mensaje' => 'Mensaje enviado' ]);
    }
}
