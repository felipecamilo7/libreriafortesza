<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    /**
* @OA\Info(title="API Usuarios", version="1.0")
*
* @OA\Server(url="http://swagger.local")
*/

   /**
 * @OA\Get(
 *     path="/login",
 *     summary="Loguear usuario",
 *   
 *     @OA\Response(
 *         response=422,
 *         description="Validation error",
 *     ),
 * )
 */

    public function login(Request $request)
    {
   
        $credenciales = $request->only('email','password');
        
     


        if (Auth::attempt($credenciales)) {
            $token = $request->user()->createToken('token-de-acceso')->accessToken;
            return response()->json(['token' => $token]);
            
        } else {
            return response()->json(['error' => 'No autorizado'], 401);
        }
    }
    /**
    * @OA\Get(
    *     path="/verUsuario",
    *     summary="ver Usuario Logueado",
    *     @OA\Response(
    *         response=200,
    *         description="ver Usuario Logueado"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function verUsuario(Request $request)
    {
        $user = User::all();


        return $user;

        // $user contiene el usuario autenticado
    }
    public function logout(){
        Auth::logout();
    }
}
